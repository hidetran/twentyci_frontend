export const appApiUrl = "http://localhost:6969/"

export function fetchPosts(token) {
    return new Promise((resolve, reject) => {
        fetch(appApiUrl + "post/fetchPost",
            {
                method: 'POST',
                body: JSON.stringify({}),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'x-token': token
                }
            })
            .then(response => response.json())
            .then(resData => resolve(resData))
    });
}