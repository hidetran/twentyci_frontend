export const appApiUrl = "http://localhost:6969/"

export function verifyLogin(loginData) {
    return new Promise((resolve, reject) => {
        fetch(appApiUrl + "auth/verifyLogin",
            {
                method: 'POST',
                body: JSON.stringify({ loginData: loginData }),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then(response => response.json())
            .then(resData => resolve(resData))
    });
}
