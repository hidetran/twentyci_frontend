import React from 'react';
import './PostsComp.css'
import PostManagement from './../../framework/PostManagement'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState } from 'draft-js';

import { Container, Row, Button, Form, FormGroup, Label, Input } from 'reactstrap'

//import widgets
import Header from './../../widgets/Header'
import Footer from './../../widgets/Footer'


class NewPost extends React.Component {
    // eslint-disable-next-line
    constructor() {
        super();
        this.state = {
            post_title: "",
            post_description: "",
            post_thumbnail: "",
            post_content_html: "",
            editorState: EditorState.createEmpty()
        }
    }

    onInputDataChange(evt) {
        this.setState({
            [evt.target.name]: evt.target.value
        });
    }

    onEditorStateChange = (editorState) => {
        this.setState({
            "post_content_html": editorState
        });
    }

    onPostSave() {
        console.log(this.state);
        
    }

    render() {
        return (
            <Container fluid={false}>
                <Row>
                    <Header />
                </Row>
                <Row className="pull-right pa-10">
                    <Button onClick={this.onPostSave.bind(this)} color="info" size="sm" className="mr-10">+ Save & Publish</Button>
                    <Button color="default" size="sm">- Discard</Button>
                </Row>
                <Row className="w-100">
                    <FormGroup className="w-100">
                        <Label for="post_title">Title</Label>
                        <Input type="text" name="post_title" id="post_title" placeholder="post title" value={this.state.post_title} onChange={this.onInputDataChange.bind(this)} className="gray-input" />
                    </FormGroup>
                </Row>
                <Row className="w-100">
                    <FormGroup className="w-100">
                        <Label for="post_title">Short Description</Label>
                        <Input type="textarea" name="post_description" id="post_description" placeholder="post content" value={this.state.post_description} onChange={this.onInputDataChange.bind(this)} className="gray-input" />
                    </FormGroup>
                </Row>
                <Row className="w-100 gray-input">
                    <FormGroup className="w-100">
                        <Editor
                            editorState={this.state.post_content_html}
                            toolbarClassName="toolbarClassName"
                            wrapperClassName="wrapperClassName"
                            editorClassName="editorClassName"
                            onEditorStateChange={this.onEditorStateChange}
                        />
                    </FormGroup>
                </Row>
                <Row>
                    <Footer />
                </Row>
            </Container>
        )
    }
}

export default NewPost;