import React from 'react';
import './PostsComp.css'
import PostManagement from './../../framework/PostManagement'
import Authenticate from './../../framework/Authenticate'
import { Link } from 'react-router-dom'

import { Container, Row, Button } from 'reactstrap';

//import widgets
import Header from './../../widgets/Header'
import Footer from './../../widgets/Footer'


class PostsComp extends React.Component {
  // eslint-disable-next-line
  constructor() {
    super();
  }
  componentDidMount() {
    //Fetch my post here
    PostManagement.fetchPosts((Authenticate.token), () => {

    })
  }
  render() {
    return (
      <Container fluid={false}>
        <Row>
          <Header />
        </Row>
        <Row className="pull-right pa-10">
          {
            PostManagement.myPost.length > 0 ?
              ''
              :
              ''
          }
          <Link to="/manage-post/new" color="info"className="btn btn-info btn-sm mr-10">+ New Post</Link>
          <Button color="danger" size="sm">- Delete</Button>
        </Row>
        <Row>
          
        </Row>
        <Row>
          <Footer />
        </Row>
      </Container>
    )
  }
}

export default PostsComp;