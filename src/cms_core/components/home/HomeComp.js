import React from 'react';
import { Container, Row } from 'reactstrap';

import './HomeComp.css'

//import widgets
import Header from './../../widgets/Header'
import Footer from './../../widgets/Footer'


class HomeComp extends React.Component {
  // eslint-disable-next-line
  constructor() {
    super();
  }

  render() {
    return (
      <Container fluid={true}>
        <Row>
          <Header />
        </Row>
        <Row>
         <Footer />
        </Row>
      </Container>
    )
  }
}

export default HomeComp;