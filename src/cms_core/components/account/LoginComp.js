import React from 'react';
import logo from './../../../logo.svg'

import { Redirect } from 'react-router-dom'
import Authenticate from './../../framework/Authenticate'
import { Container, Row, Button, Form, FormGroup, Label, Input } from 'reactstrap'

class LoginComp extends React.Component {
  // eslint-disable-next-line
  constructor(props) {
    super(props);

    this.state = {
      redirectToReferrer: false,
      loginData: {
        username: '',
        password: ''
      }
    }
  }

  authenticate() {
    Authenticate.authenticate((this.state.loginData), () => {
      this.setState(() => ({
        redirectToReferrer: true
      }))
    })
  }

  onInputDataChange(evt) {
    this.setState({
      loginData: {
        ...this.state.loginData,
        [evt.target.name]: evt.target.value
      }
    });
  }
  render() {
    const { from } = this.props.location.state || { from: { pathname: '/' } }
    const { redirectToReferrer } = this.state

    if (redirectToReferrer === true) {
      return <Redirect to={from} />
    }

    return (
      <Container>
        <Row>
          <Form className="login-form">
            <FormGroup>
              <img src={logo} alt="twentyCi" height="50" />
              <legend className="login-heading">LOGIN FORM</legend>
              <Label for="username">Username</Label>
              <Input type="text" name="username" id="username" placeholder="demo" value={this.state.loginData.username} onChange={this.onInputDataChange.bind(this)} />
            </FormGroup>
            <FormGroup>
              <Label for="password">Password</Label>
              <Input type="password" name="password" id="password" placeholder="demo" value={this.state.loginData.password} onChange={this.onInputDataChange.bind(this)} />
            </FormGroup>
            <Button color="info" onClick={this.authenticate.bind(this)}>LOGIN</Button>
          </Form>
        </Row>
      </Container>
    )
  }
}

export default LoginComp;