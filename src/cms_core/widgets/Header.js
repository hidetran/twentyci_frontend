import React from 'react';
import logo from './../../logo.svg'

import { Link } from 'react-router-dom'
import {
  Container,
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink
} from 'reactstrap';

//import other widgets
import AuthenticatedMenu from './AuthenticatedMenu'

class Header extends React.Component {
  // eslint-disable-next-line
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    return (
      <Container className="header">
        <Navbar color="light" light expand={true}>
          <NavbarBrand href="/"><img src={logo} alt="twentyCi" height="50" /></NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <Link to="/" className="nav-link">HOME</Link>
              </NavItem>
              <NavItem>
                <NavLink href="/news" disabled={true}>NEWS</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/contact"  disabled={true}>CONTACT US</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/help"  disabled={true}>HELMP</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/terms"  disabled={true}>TERMS</NavLink>
              </NavItem>
              <AuthenticatedMenu />
            </Nav>
          </Collapse>
        </Navbar>
      </Container>
    )
  }
}

export default Header;
