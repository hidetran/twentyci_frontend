import React from 'react';
import Authenticate from './../framework/Authenticate'
import { Link } from 'react-router-dom'
import {
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavLink
} from 'reactstrap';

class AuthenticatedMenu extends React.Component {
  // eslint-disable-next-line
  constructor() {
    super();
  }

  render() {
    return (
      Authenticate.isAuthenticated === true
        ?
        <UncontrolledDropdown nav inNavbar>
          <DropdownToggle nav caret>
            Welcome, {Authenticate.fullName}
        </DropdownToggle>
          <DropdownMenu right>
            <DropdownItem>
              <Link to="/manage-post" className="nav-link">Manage Posts</Link>
            </DropdownItem>
            <DropdownItem divider />
            <DropdownItem>
              <Link to="/" className="nav-link" onClick={() => {
                Authenticate.signout()
              }}>Sign out</Link>
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
        :
        <UncontrolledDropdown nav inNavbar>
          <DropdownToggle nav caret>
            Login/Register
          </DropdownToggle>
          <DropdownMenu right>
            <DropdownItem>
              <Link to="/login" className="nav-link">Login</Link>
            </DropdownItem>
            <DropdownItem divider />
            <DropdownItem>
              <NavLink href="/register" disabled={true}>Register</NavLink>
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
    )
  }
}

export default AuthenticatedMenu;
