import { verifyLogin } from '../services/AuthenticateService'

const Authenticate = {
    isAuthenticated: false,
    fullName: '',
    token: '', //those information should be stored in localStorage

    authenticate(loginData, callback) {
        //call authenticate service function here
        verifyLogin(loginData).then((result) => {
          if(result.error){
            alert(result.message)
          }else{
            this.isAuthenticated = true; 
            this.fullName = result.data.fullName;
            this.token = result.data.token;
            callback();
          }
        })
    },

    signout(callback) {
        //call signout service function here
        this.isAuthenticated = false;
        setTimeout(callback, 100);
    }
}
export default Authenticate;