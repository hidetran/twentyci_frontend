import React from 'react';
import { withRouter } from 'react-router'
import CmsContext from './CmsContext'

class CmsProvider extends React.Component {
  constructor() {
    super();
    this.state = { someKey: 'someValue' };
  }

  render() {
    return (
            <CmsContext.Provider value={{
                                state: this.state,
                                updateLanguage: (country) => this.updateLanguage(country)
                            }}> 
                            {
                                this.props.children
                            }
            </CmsContext.Provider>
    )
  }
}

export default withRouter(CmsProvider);