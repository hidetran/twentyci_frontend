import React from 'react';

const CmsContext = React.createContext();

export default CmsContext;