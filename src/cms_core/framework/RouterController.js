import React from 'react';
import {  Route, Redirect } from 'react-router-dom';

import Authenticate from './Authenticate'

const ProtectedRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={(props) => (
    Authenticate.isAuthenticated === true
      ? <Component {...props} />
      : <Redirect to={{pathname: '/login', state: { from: props.location }}} />
  )} />
)

export default ProtectedRoute