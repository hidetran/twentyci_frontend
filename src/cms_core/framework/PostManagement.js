import { fetchPosts } from '../services/PostService'

//CRUD post end-point sheld

const PostManagement = {
    myPost: [],
    fetchPosts(token, callback) {
        fetchPosts(token).then((result) => {
          if(!result.error){
            this.myPost = result.data
            callback();
          }
        })
    }
}
export default PostManagement;