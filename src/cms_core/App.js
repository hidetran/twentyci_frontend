import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import { CookiesProvider, withCookies } from 'react-cookie';
import CmsProvider from './framework/CmsProvider'

//Import components for router
import HomeComp from './components/home/HomeComp'
import PostsComp from './components/post/PostsComp'
import NewPost from './components/post/NewPost'
import LoginComp from './components/account/LoginComp'

import ProtectedRoute from './framework/RouterController'

class App extends Component {
  render() {
    return (
      <CookiesProvider>
        <BrowserRouter>
          <CmsProvider>
            <div>
              <Route exact path="/" component={HomeComp} />
              <Route exact path="/login" component={LoginComp} />
              <ProtectedRoute exact path="/manage-post" component={PostsComp} />
              <ProtectedRoute exact path="/manage-post/new" component={NewPost} />
            </div>
          </CmsProvider>
        </BrowserRouter>
      </CookiesProvider>
    );
  }
}

export default withCookies(App);
