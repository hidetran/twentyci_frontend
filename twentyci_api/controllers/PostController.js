var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var _ = require('lodash');
var VerifyToken = require('../helpers/VerifyToken');
var uuid = require('uuid');
var fs = require('fs');

var posts = require('../data/posts.json');


router.use(bodyParser.json({ limit: '50mb' }));

router.use(bodyParser.urlencoded({
    extended: true,
    limit: '50mb'
}));

/* API Endpoint method */
router.post('/', function (req, res, next) {
    return res.status(200).send({
        error: false,
        message: "twentyCiCMS Posts API endpoint is running ...",
        data: null
    });
});

router.post('/fetchPost', VerifyToken, function (req, res) {    
    //fake filter post here
    let myPost = _.filter(posts, item => item.author === req.username);
    return res.status(200).send({
        error: false,
        message: "Posts fetched successfully",
        data: myPost
    });

});

router.post('/newpost', VerifyToken, function (req, res) {    
    if(req.body.postData.post_title.length >  0 && req.body.postData.post_description.length > 0 && req.body.postData.post_content_html.length > 0){

        //irnore some html check and validation
        req.body.postData.id = uuid.v4();
        req.body.postData.created_date = new Date();
        req.body.postData.author = req.username;
        req.body.postData.is_deleted = false;
        
        posts.push(req.body.postData);
        
        return res.status(200).send({
            error: false,
            message: "Post saved successfully",
            data: req.body.postData
        });   
    }else{
        return res.status(200).send({
            error: true,
            message: "Cannot create your post, please enter some text strings",
            data: null
        });   
    }
});

/* API Endpoint method */
module.exports = router;