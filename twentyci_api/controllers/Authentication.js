var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var _ = require('lodash');
var jwt = require('jsonwebtoken');

router.use(bodyParser.json({ limit: '50mb' }));

router.use(bodyParser.urlencoded({
    extended: true,
    limit: '50mb'
}));

/* API Endpoint method */
router.post('/', function (req, res, next) {
    return res.status(200).send({
        error: false,
        message: "twentyCiCMS Authentication API endpoint is running ...",
        data: null
    });
});

//runtime user dataa
let users = [
    { username: 'demo', password: 'demo', fullName: 'twentyCi CMS Admin'},
    { username: 'demo2', password: 'demo2', fullName: 'twentyCi CMS Author'}
]
//runtime secret
let secretKey = "this_should_be_more_complex_key";

router.post('/verifyLogin', function (req, res) {
    //fake valid loginData here
    var userInfo = _.filter(users, item => item.username === req.body.loginData.username && item.password === req.body.loginData.password);
  
    if(userInfo.length > 0){
        //generate token for this user
        var token = jwt.sign({ username: userInfo[0].username.toLowerCase() }, secretKey, { expiresIn: 86400 }); //just for one day

        return res.status(200).send({
            error: false,
            message: "Login successfully",
            data: {
                username: userInfo[0].username,
                fullName: userInfo[0].fullName,
                token: token
            }
        });
    }else{
        return res.status(200).send({
            error: true,
            message: "Login Failed. Please try again later !",
            data: null
        });
    }
});

/* API Endpoint method */
module.exports = router;