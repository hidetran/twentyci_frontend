var express = require('express');
var twentyCiCMS = express();
global.__root = __dirname + '/';

twentyCiCMS.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, x-token");
    next();
});

twentyCiCMS.get('/', function (req, res) {
    return res.status(200).send({
        error: false,
        message: "twentyCiCMS API endpoint is running ...",
        data: hash
    });
});


var AuthController = require(__root + 'controllers/Authentication');
var PostController = require(__root + 'controllers/PostController');

twentyCiCMS.use('/auth', AuthController);
twentyCiCMS.use('/post', PostController);

module.exports = twentyCiCMS;