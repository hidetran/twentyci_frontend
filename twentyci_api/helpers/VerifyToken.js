var jwt = require('jsonwebtoken');
var secretKey = "this_should_be_more_complex_key";

function verifyToken(req, res, next) {
    var token = req.headers['x-token'];

    if (!token)
        return res.status(202).send({ error: true, auth: false, message: 'No authenticationtoken provided.' });

    jwt.verify(token, secretKey, function (err, decoded) {
        if (err)
            return res.status(202).send({ error: true, auth: false, message: 'Failed to authenticate.' });
        req.username = decoded.username.toLowerCase();
        next();
    });

}

module.exports = verifyToken;