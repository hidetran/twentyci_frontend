This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

In this project, I am using ReactJS Context API, Router V4 with protected router, fetch, ...

### Installation
```bash
> git clone https://gitlab.com/hidetran/twentyci_frontend.git
> cd twentyci_frontend
> npm install
> cd twemtyci_api
> npm install
> node twentyci.start.js
```
Open new command line window and navigate to twentyci_frontend folder

```bash
> npm start
```

Open web browser and enter http://localhost:3000

### Contact
hidetran@gmail.com

### Copyrights
MIT